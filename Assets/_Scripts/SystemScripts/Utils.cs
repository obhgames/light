﻿public class Utils
{
    public static class Levels
    {
        public const string MAINMENU = "scnMainMenu";
        public const string SCHOOL = "scnSchool";
    }

    public static class Tags
    {
        public const string PLAYER = "Player";
        public const string GAME_CONTROLLER = "GameController";
        public const string TERRAIN = "Terrain";
    }

    public static class AnimationParamNames
    {
        public static class MainMenu
        {

        }

        public static class Gameplay
        {
            public static class Player
            {
                public static class FlashLight
                {
                    public const string WALK = "Walking";
                    public const string WALK_BLEND = "Speed";
                    public const string JUMP = "Jump";
                }
            }
        }
    }

    public static class Input
    {
        public static class Axis
        {
            public const string HORIZONTAL = "Horizontal";
            public const string VERTICAL = "Vertical";
            public const string MOUSE_X = "Mouse X";
            public const string MOUSE_Y = "Mouse Y";
            public const string JUMP = "Jump";
            public const string SPRINT = "Sprint";
            public const string ESCAPE = "Cancel";
        }
    }

    public static class EventManagerActionNames
    {
        public static class Player
        {
            public static class Audio
            {
                public const string PLAY_STEP_SOUND = "Player_Audio_PlayStep";
                public const string PAUSE_STEP_SOUND = "Player_Audio_StopStep";
                public const string SET_AUDIO_SPEED_TO_NORMAL = "Player_Audio_SetAudioSpeedToNormal";
                public const string SET_AUDIO_SPEED_TO_QUICK = "Player_Audio_SetAudioSpeedToQuick";
            }

            public static class Animator
            {
                public static class FlashLight
                {
                    public const string PLAY_WALK_ANIMATION = "Player_Flashlight_Play_Walk";
                    public const string STOP_WALK_ANIMATION = "Player_Flashlight_Stop_Walk";
                    public const string INCREASE_SPEED = "Player_Flashlight_Increase_Speed";
                    public const string DECREASE_SPEED = "Player_Flashlight_Decrease_Run";
                }
            }
        }
    }
}
