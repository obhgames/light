﻿using UnityEngine;

public class GameController : Singleton<GameController>
{
    public PlayState playState = PlayState.PLAY;

    public enum PlayState
    {
        PAUSE,
        PLAY
    }

    void OnEnable()
    {
        playState = PlayState.PLAY;
    }

    void Update()
    {
        if (Input.GetButtonUp(Utils.Input.Axis.ESCAPE))
            EscapePressed();
    }

    private void EscapePressed()
    {
        switch (playState)
        {
            case PlayState.PAUSE:
                playState = PlayState.PLAY;
                //TODO
                break;

            case PlayState.PLAY:
                playState = PlayState.PAUSE;
                //TODO
                break;
        }
    }
}
