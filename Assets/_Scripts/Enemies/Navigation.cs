﻿using UnityEngine;

public class Navigation : MonoBehaviour
{
    private NavMeshAgent nav;
    private Transform player;

    void Awake()
    {
        nav = GetComponent<NavMeshAgent>();
        player = GameObject.FindWithTag(Utils.Tags.PLAYER).transform;
    }

    void Update()
    {
        if (GameController.Instance.playState == GameController.PlayState.PLAY)
            nav.SetDestination(player.position);
    }
}
