﻿using UnityEngine;

public class AudioController : MonoBehaviour
{
    private AudioSource audioSource;
    private bool isWalking = true;

    [SerializeField]
    private AudioClip sndWalk;

    [SerializeField]
    private AudioClip sndRun;

    void OnEnable()
    {
        audioSource = GetComponent<AudioSource>();

        EventManager.StartListening(Utils.EventManagerActionNames.Player.Audio.PLAY_STEP_SOUND, Play);
        EventManager.StartListening(Utils.EventManagerActionNames.Player.Audio.PAUSE_STEP_SOUND, Stop);
        EventManager.StartListening(Utils.EventManagerActionNames.Player.Audio.SET_AUDIO_SPEED_TO_QUICK, SetAudioFaster);
        EventManager.StartListening(Utils.EventManagerActionNames.Player.Audio.SET_AUDIO_SPEED_TO_NORMAL, SetAudioNormal);
    }

    void OnDisable()
    {
        EventManager.StopListening(Utils.EventManagerActionNames.Player.Audio.PLAY_STEP_SOUND, Play);
        EventManager.StopListening(Utils.EventManagerActionNames.Player.Audio.PAUSE_STEP_SOUND, Stop);
        EventManager.StopListening(Utils.EventManagerActionNames.Player.Audio.SET_AUDIO_SPEED_TO_QUICK, SetAudioFaster);
        EventManager.StopListening(Utils.EventManagerActionNames.Player.Audio.SET_AUDIO_SPEED_TO_NORMAL, SetAudioNormal);
    }

    private void Play()
    {
        if (!audioSource.isPlaying)
            audioSource.PlayDelayed(0.2f);
    }

    private void Stop()
    {
        if (audioSource.isPlaying)
            audioSource.Stop();
    }

    private void SetAudioFaster()
    {
        if (isWalking)
        {
            isWalking = !isWalking;
            audioSource.clip = sndRun;
            RestartAudio();
        }
    }

    private void SetAudioNormal()
    {
        if (!isWalking)
        {
            isWalking = !isWalking;
            audioSource.clip = sndWalk;
            RestartAudio();
        }
    }

    private void RestartAudio()
    {
        Stop();
        Play();
    }
}
