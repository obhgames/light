﻿using UnityEngine;
using System.Collections;

public class PlayerController : Singleton<PlayerController>
{
    [SerializeField]
    [Range(0, 100f)]
    public float energy = 100f;

    void Start()
    {
        StartCoroutine(RechargeEnergy());
    }

    private IEnumerator RechargeEnergy()
    {
        while (true)
        {
            if (energy < 100)
                energy += 1f;
            yield return new WaitForSeconds(.5f);
        }
    }
}
