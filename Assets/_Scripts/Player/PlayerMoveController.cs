﻿using UnityEngine;

[AddComponentMenu("Player/FPS Control")]
public class PlayerMoveController : MonoBehaviour
{
    private enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }

    [SerializeField]
    [Range(1f, 40f)]
    private float moveSpeed;
    [SerializeField]
    [Range(50f, 100f)]
    private float rotateSpeed;

    [SerializeField]
    private RotationAxes axes = RotationAxes.MouseXAndY;

    [SerializeField]
    [Range(1f, 360f)]
    private float sensitivityX = 15F;
    [SerializeField]
    [Range(1f, 360f)]
    private float sensitivityY = 15F;

    [SerializeField]
    [Range(1f, 360f)]
    public float minimumX = -360F;
    [SerializeField]
    [Range(1f, 360f)]
    public float maximumX = 360F;

    [SerializeField]
    [Range(30f, 120f)]
    private float minimumY = -60F;
    [SerializeField]
    [Range(30f, 120f)]
    private float maximumY = 60F;

    private float rotationY = 0F;

    [SerializeField]
    [Range(0.1f, 2f)]
    private float sprintOffset = 1f;

    private CharacterController characterController;

    [SerializeField]
    [Range(4f, 10f)]
    private float jumpSpeed = 8.0F;
    [SerializeField]
    [Range(10f, 30f)]
    private float gravity = 20.0F;

    private Vector3 moveDirection = Vector3.zero;

    void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        if (GameController.Instance.playState == GameController.PlayState.PLAY)
        {
            KeyboardInput();
            MouseInput();
        }
    }

    private void KeyboardInput()
    {
        float sprint = 1f;

        bool isRunning = Input.GetButton(Utils.Input.Axis.SPRINT) && PlayerController.Instance.energy > 0;

        if (characterController.isGrounded)
        {
            if (isRunning)
            {
                PlayerController.Instance.energy -= Time.deltaTime * 20f;
                sprint = sprintOffset * 2;
            }
            else
            {
                sprint = sprintOffset;
            }

            moveDirection = new Vector3(Input.GetAxis(Utils.Input.Axis.HORIZONTAL), 0, Input.GetAxis(Utils.Input.Axis.VERTICAL));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= moveSpeed;
            if (Input.GetButton(Utils.Input.Axis.JUMP))
                moveDirection.y = jumpSpeed;
        }

        moveDirection.y -= gravity * Time.deltaTime;
        characterController.Move(moveDirection * Time.deltaTime * sprint);
        PlayerMoveStatusChecker(isRunning, characterController.isGrounded);
    }

    private void MouseInput()
    {
        if (axes == RotationAxes.MouseXAndY)
        {
            float rotationX = transform.localEulerAngles.y + Input.GetAxis(Utils.Input.Axis.MOUSE_X) * sensitivityX;

            rotationY += Input.GetAxis(Utils.Input.Axis.MOUSE_Y) * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }
        else if (axes == RotationAxes.MouseX)
        {
            transform.Rotate(0, Input.GetAxis(Utils.Input.Axis.MOUSE_X) * sensitivityX, 0);
        }
        else
        {
            rotationY += Input.GetAxis(Utils.Input.Axis.MOUSE_Y) * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
        }
    }

    private void PlayerMoveStatusChecker(bool isRunning, bool isGrounded)
    {
        if ((moveDirection.x != 0 || moveDirection.z != 0) && isGrounded)
        {
            if (isRunning)
            {
                EventManager.TriggerEvent(Utils.EventManagerActionNames.Player.Audio.SET_AUDIO_SPEED_TO_QUICK);
                EventManager.TriggerEvent(Utils.EventManagerActionNames.Player.Animator.FlashLight.INCREASE_SPEED);
                return;
            }

            EventManager.TriggerEvent(Utils.EventManagerActionNames.Player.Audio.SET_AUDIO_SPEED_TO_NORMAL);
            EventManager.TriggerEvent(Utils.EventManagerActionNames.Player.Animator.FlashLight.PLAY_WALK_ANIMATION);
            EventManager.TriggerEvent(Utils.EventManagerActionNames.Player.Animator.FlashLight.DECREASE_SPEED);
            EventManager.TriggerEvent(Utils.EventManagerActionNames.Player.Audio.PLAY_STEP_SOUND);
        }
        else
        {
            EventManager.TriggerEvent(Utils.EventManagerActionNames.Player.Audio.PAUSE_STEP_SOUND);
            EventManager.TriggerEvent(Utils.EventManagerActionNames.Player.Animator.FlashLight.STOP_WALK_ANIMATION);
        }
    }
}
