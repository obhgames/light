﻿using UnityEngine;

public class FlashlightAnimatorController : MonoBehaviour
{
    private Animator animator;
    private float speed = 0f;
    private bool isWalking = false;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    void OnEnable()
    {
        EventManager.StartListening(Utils.EventManagerActionNames.Player.Animator.FlashLight.PLAY_WALK_ANIMATION, Walk);
        EventManager.StartListening(Utils.EventManagerActionNames.Player.Animator.FlashLight.STOP_WALK_ANIMATION, Stop);
        EventManager.StartListening(Utils.EventManagerActionNames.Player.Animator.FlashLight.INCREASE_SPEED, IncreaseVelocity);
        EventManager.StartListening(Utils.EventManagerActionNames.Player.Animator.FlashLight.DECREASE_SPEED, DecreaseVelocity);
    }

    void OnDisable()
    {
        EventManager.StopListening(Utils.EventManagerActionNames.Player.Animator.FlashLight.PLAY_WALK_ANIMATION, Walk);
        EventManager.StopListening(Utils.EventManagerActionNames.Player.Animator.FlashLight.STOP_WALK_ANIMATION, Stop);
        EventManager.StopListening(Utils.EventManagerActionNames.Player.Animator.FlashLight.INCREASE_SPEED, IncreaseVelocity);
        EventManager.StopListening(Utils.EventManagerActionNames.Player.Animator.FlashLight.DECREASE_SPEED, DecreaseVelocity);
    }

    private void Walk()
    {
        isWalking = animator.GetBool(Utils.AnimationParamNames.Gameplay.Player.FlashLight.WALK);
        if (!isWalking)
            animator.SetBool(Utils.AnimationParamNames.Gameplay.Player.FlashLight.WALK, !isWalking);
    }

    private void Stop()
    {
        isWalking = animator.GetBool(Utils.AnimationParamNames.Gameplay.Player.FlashLight.WALK);
        if (isWalking)
            animator.SetBool(Utils.AnimationParamNames.Gameplay.Player.FlashLight.WALK, !isWalking);
    }

    private void IncreaseVelocity()
    {
        isWalking = animator.GetBool(Utils.AnimationParamNames.Gameplay.Player.FlashLight.WALK);
        if (speed < 1f && isWalking)
        {
            speed += Time.deltaTime;
            animator.SetFloat(Utils.AnimationParamNames.Gameplay.Player.FlashLight.WALK_BLEND, speed);
        }
    }

    private void DecreaseVelocity()
    {
        isWalking = animator.GetBool(Utils.AnimationParamNames.Gameplay.Player.FlashLight.WALK);
        if (speed > 0f && isWalking)
        {
            speed -= Time.deltaTime;
            animator.SetFloat(Utils.AnimationParamNames.Gameplay.Player.FlashLight.WALK_BLEND, speed);
        }
    }
}
