﻿using UnityEngine;
using System.Collections;

public class UIEvents : MonoBehaviour
{
    public void OnClickPlay()
    {
        Debug.Log("PLAY");
		Application.LoadLevel (Utils.Levels.SCHOOL);
    }

    public void OnClickOptions()
    {
        Debug.Log("OPTIONS");
    }

    public void OnClickExit()
    {
        Debug.Log("EXIT");
        Application.Quit();
    }
}
