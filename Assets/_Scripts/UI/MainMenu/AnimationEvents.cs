﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnimationEvents : MonoBehaviour
{
    private Animator animator;
    private RectTransform pButtons;
    private RectTransform imgLight;

	void Awake ()
	{
        animator = (Animator)GetComponent<Animator>();
		pButtons = (RectTransform)this.gameObject.transform.GetChild (0);
        imgLight = (RectTransform)this.gameObject.transform.GetChild(1);        

        pButtons.gameObject.SetActive(false);
        imgLight.gameObject.SetActive(false);
	}

    private void LogoAnimationFinished ()
	{
        pButtons.gameObject.SetActive(true);
        imgLight.gameObject.SetActive(true);
    }
}
