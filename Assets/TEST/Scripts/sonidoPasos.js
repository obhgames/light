#pragma strict

var stepInterval: float = 0.3; // min interval between steps
var stepLength: float = 0.5; // distance walked by each step
var footsteps: AudioClip[]; // drag step sounds here
 
private var stepTime: float;
private var lastStep: Vector3;
 
function Start(){

    lastStep = transform.position; // initialize lastStep position


}
 
function Update () {
    // if walked a distance >= stepLength...
	    if (Vector3.Distance(transform.position, lastStep) >= stepLength){
	       if (Time.time > stepTime){ // and it's time to play a new footstep...
	        
	          // play a randomly selected sound:
	        
	          GetComponent.<AudioSource>().PlayOneShot(footsteps[24]);
	         
	         lastStep = transform.position; // update lastStep and stepTime:
	         
	         stepTime = Time.time + stepInterval;
	       }
	    }
}