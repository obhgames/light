using UnityEngine;
using System.Collections;

public class abrirpuertas : MonoBehaviour {


	public Transform player;
	public Transform grupoControl;

	public AnimationClip animacionAbrir;
	public AnimationClip animacionCerrar;

	private bool  puertaAbierta = false;

	public float distApertura = 1.5f;
	public float distCierre = 4;
	
	private Animation _animation;


	void Start (){

	_animation = GetComponent<Animation>();
		if(!_animation)
			Debug.Log("El carácter que desea controlar no tiene animaciones. Su movimiento podria ser extraño");

	}

	void Update (){
		if(player != null){
			if(((player.position-grupoControl.transform.position).magnitude<distApertura) && puertaAbierta==false){
			
				puertaAbierta = true;
				_animation.Play(animacionAbrir.name);
			
			}
			
			if(((player.position-grupoControl.transform.position).magnitude>distCierre) && puertaAbierta==true){

				puertaAbierta = false;
				_animation.Play(animacionCerrar.name);
			
			}
		}
	}
}