using UnityEngine;
using System.Collections;

public class sonidoPasos : MonoBehaviour {


float stepInterval = 0.3f; // min interval between steps
float stepLength = 0.5f; // distance walked by each step
AudioClip[] footsteps; // drag step sounds here
 
private float stepTime;
private Vector3 lastStep;
 
void Start (){

    lastStep = transform.position; // initialize lastStep position
}
 
void Update (){
    // if walked a distance >= stepLength...
	    if (Vector3.Distance(transform.position, lastStep) >= stepLength){
	       if (Time.time > stepTime){ // and it's time to play a new footstep...
	        
	          // play a randomly selected sound:
	        
	          GetComponent<AudioSource>().PlayOneShot(footsteps[24]);
	         
	         lastStep = transform.position; // update lastStep and stepTime:
	         
	         stepTime = Time.time + stepInterval;
	       }
	    }
}
}