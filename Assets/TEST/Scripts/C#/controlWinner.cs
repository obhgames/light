﻿using UnityEngine;
using System.Collections;

public class controlWinner : MonoBehaviour {


	//GUIStyle
	[SerializeField]
	private GUIStyle boton_style;
	[SerializeField]
	private GUIStyle OSOS;
	[SerializeField]
	private GUIStyle titulo_style;
	[SerializeField]
	private GUIStyle titulo_verde_style;
/*	[SerializeField]
	private GUIStyle fondo;
	[SerializeField]
	private GUIStyle flores;
*/


	private Texture2D youWinAtras;
	private float y;
		private float pantalla;

//	private float  scrollSpeed  = 0.5;
	// Use this for initialization
	void Start () {
	
		 youWinAtras= Resources.Load("winner/youwintras") as Texture2D;
		//titulo_verde_style.normal.background = youWinAtras;
		StartCoroutine(cambiar());
		y = Screen.height*0;
	}
	
	// Update is called once per frame
	void Update () {
		boton_style.fontSize = Screen.width / 40;
		titulo_style.fontSize = Screen.width / 9;
		titulo_verde_style.fontSize = Screen.width / 10;

		//titulo_verde_style.normal.background = youWinAtras;


		//float offset = Time.time * scrollSpeed;
	//	y += Screen.height*1/300;
	//	pantalla = Screen.height;
	
	}
	void OnGUI(){

		//GUI.Label(new Rect(Screen.width*0, Screen.height*0, Screen.width, Screen.height),"",fondo);
		//GUI.Label(new Rect(Screen.width*0, y, Screen.width, Screen.height),"",flores);
		GUI.Label ( new Rect(Screen.width*3/12, Screen.height*2/12, Screen.width*7/12, Screen.height*5/12),"",titulo_verde_style);
		GUI.Label ( new Rect(Screen.width*3/12, Screen.height*2/12, Screen.width*7/12, Screen.height*5/12),"",titulo_style);


		GUI.Label ( new Rect(Screen.width*1/15, Screen.height*1/15, Screen.width*3/15, Screen.width*3/15),"",OSOS);
		GUI.Label ( new Rect(Screen.width*12/15, Screen.height*1/12, Screen.width*3/15, Screen.width*3/15),"",OSOS);

		if(GUI.Button(new Rect(Screen.width*5/12, Screen.height*6/12, Screen.width*2/12, Screen.height*1/12),"Main menu",boton_style))
		{
			//Caching.CleanCache();
			Application.LoadLevel("menuInicial");
		}
		
		if(GUI.Button( new Rect(Screen.width*5/12, Screen.height*8/12, Screen.width*2/12, Screen.height*1/12),"Quit",boton_style)){
			
			Application.Quit();
		}	
	}
	IEnumerator cambiar()
	{
		while(true)
		{
			titulo_verde_style.normal.background = youWinAtras;
			yield return new WaitForSeconds(1f);
			titulo_verde_style.normal.background = null;
			yield return new WaitForSeconds(1f);
		}
	}
		
		
}		