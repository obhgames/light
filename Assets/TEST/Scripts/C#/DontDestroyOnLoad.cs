using UnityEngine;
using System.Collections;

public class DontDestroyOnLoad : MonoBehaviour {


void Awake (){

    // see if we've got game music still playing
    
    GameObject gameMusic = GameObject.Find("MenuMusic");
    
    if (gameMusic) {
    
        // kill game music
        
        Destroy(gameObject);
    }
    else
    {
    // make sure we survive going to different scenes
    DontDestroyOnLoad(gameObject);}
    
}
}