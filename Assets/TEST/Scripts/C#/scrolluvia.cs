using UnityEngine;
using System.Collections;

public class scrolluvia : MonoBehaviour {


	[SerializeField]
	private  float scrollSpeed = 0.5f;

	void Update (){

	float offset = Time.time * scrollSpeed;
	GetComponent<Renderer>().material.SetTextureOffset ("_MainTex", new Vector2(0,offset));

	}

}