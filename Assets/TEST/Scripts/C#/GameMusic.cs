using UnityEngine;
using System.Collections;

public class GameMusic : MonoBehaviour {


void Awake (){
    // see if we've got menu music still playing
    GameObject menuMusic = GameObject.Find("GameMusic");
    if (menuMusic) {
        // kill menu music
        Destroy(menuMusic);
    }
    // make sure we survive going to different scenes
    DontDestroyOnLoad(gameObject);
}
}