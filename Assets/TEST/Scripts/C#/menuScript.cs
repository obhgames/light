using UnityEngine;
using System.Collections;

public class menuScript : MonoBehaviour {

//GameObject
	public GameObject sonidoMemu;

//GUIStyle
	[SerializeField]
	private GUIStyle titulo_style;
	[SerializeField]
	private GUIStyle boton_style;
	[SerializeField]
	private GUIStyle fondo;
	[SerializeField]
	private GUIStyle instruciones;

//Bools
	private bool  mostrar_instrucciones;
	private bool  mostrar_menu;
	private bool  mostrar_levels;

	void Start (){

		GameObject gameMusic = GameObject.Find("MenuMusic(Clone)");
	    
	    if (!gameMusic) {
	    
	   		Instantiate(sonidoMemu,transform.position,transform.rotation);
	    }

		mostrar_menu = true;
		mostrar_instrucciones = false;
		mostrar_levels= false;
	
	}

	void Update (){

	//Actualizando fuentes
		titulo_style.fontSize = Screen.width / 15;
		boton_style.fontSize = Screen.width / 40;
			

	}

	void OnGUI (){

		GUI.Label( new Rect(0,0,Screen.width,Screen.height),"",fondo);

		if(mostrar_menu){		
		
			GUI.Label ( new Rect(Screen.width*45/120, Screen.height*1/10, Screen.width*3/12, Screen.height*3/12), "LIGHT",titulo_style);
		
			if(GUI.Button( new Rect(Screen.width*5/12, Screen.height*5/12, Screen.width*2/12, Screen.height*1/12),"Levels",boton_style)){
			
				mostrar_levels = true;
				mostrar_menu = false;
							
			}	

			if(GUI.Button( new Rect(Screen.width*5/12, Screen.height*7/12, Screen.width*2/12, Screen.height*1/12),"Instructions",boton_style)){
				mostrar_instrucciones = true;
				mostrar_menu = false;

			}
			
			if(GUI.Button( new Rect(Screen.width*5/12, Screen.height*9/12, Screen.width*2/12, Screen.height*1/12),"Quit",boton_style)){
			
				Application.Quit();
			}
			
		
		}

		if (mostrar_instrucciones){

			GUI.Label( new Rect(Screen.width*45/120, Screen.height*1/12,Screen.width*3/12,Screen.height*11/12),"",instruciones);
				
			if(GUI.Button( new Rect(Screen.width*1/12, Screen.height*1/12, Screen.width*1/12, Screen.height*1/12),"Back",boton_style)){

				mostrar_instrucciones = false;
				mostrar_menu = true;
					
			}
		}	
		if(mostrar_levels){
			
				if(GUI.Button( new Rect(Screen.width*5/12, Screen.height*5/12, Screen.width*2/12, Screen.height*1/12),"School",boton_style)){
				
					Application.LoadLevel("colegio");
				
				}
				
				if(GUI.Button( new Rect(Screen.width*1/12, Screen.height*1/12, Screen.width*1/12, Screen.height*1/12),"Back",boton_style)){	

					mostrar_levels = false;
					mostrar_menu = true;
					
				}
				
		}	
	}
}