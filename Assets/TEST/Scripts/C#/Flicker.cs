using UnityEngine;
using System.Collections;

public class Flicker : MonoBehaviour {

	[SerializeField]
	private Light luz; 						//No es necesario asignarla si se añade directamente en la luz
	[SerializeField]
	private AudioClip sonidoEncender;
	[SerializeField]
	private AudioClip sonidoApagar;

	[SerializeField]
	private float minTiempoEncendido;
	[SerializeField]
	private float maxTiempoEncendido;
	[SerializeField]
	private float minTiempoApagado;
	[SerializeField]
	private float maxTiempoApagado;

	private float actTiempo;
	private GameObject flickerSound;

private float contador;

void Awake (){
	if(!luz&&gameObject.GetComponent<Light>()){
		luz = gameObject.GetComponent<Light>();
	}
	
	actTiempo = TiempoAleatorio (minTiempoEncendido,maxTiempoEncendido);

		flickerSound= new GameObject ("FlickerSound");

}

void Update (){

	contador += Time.deltaTime;

	if(contador>=actTiempo){

		if(luz.enabled){
	
			luz.enabled = false;
		
			actTiempo = TiempoAleatorio (minTiempoApagado,maxTiempoApagado);
	
		}
		else{
	
			luz.enabled = true;
	
			actTiempo = TiempoAleatorio (minTiempoEncendido,maxTiempoEncendido);
		}
		
		if(sonidoEncender&&sonidoApagar){//Si estan los dos sonidos asignados
			Sonar(luz.enabled);
		}
		
	}
}

 float TiempoAleatorio ( float min ,float max  ){

	float tiempo = Random.Range(min,max);
	
	contador = 0;
	
	return tiempo;

}

private void Sonar ( bool encendido){

	
	
	flickerSound.transform.position = transform.position;
	
	flickerSound.AddComponent<AudioSource>();
	
	if(encendido){
	
		flickerSound.GetComponent<AudioSource>().clip = sonidoEncender;
        
	}
	else{
	
		flickerSound.GetComponent<AudioSource>().clip = sonidoApagar;
	
	}
	
	flickerSound.GetComponent<AudioSource>().Play();
	
		StartCoroutine (Esperar ());
	
	Destroy(flickerSound);

}

	IEnumerator Esperar(){

		yield return new WaitForSeconds(flickerSound.GetComponent<AudioSource>().clip.length);
	}
}