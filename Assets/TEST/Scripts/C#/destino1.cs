using UnityEngine;
using System.Collections;

public class destino1 : MonoBehaviour {


	public float distanciaVision=25;
	public Transform yoMismo;				//target al que sigue
	private NavMeshAgent enemigo;


	void Start (){

		enemigo=gameObject.GetComponent<NavMeshAgent>();
	}

	void Update (){

	if(yoMismo != null){
		   float dist= Vector3.Distance(yoMismo.position, transform.position);

			if(distanciaVision>=dist){						//si la distancia al objetivo es menor que la distancia de vision 
			
				enemigo.speed=5;							//velocidad del enemigo
				enemigo.destination=yoMismo.position;		//destino del enemigo
			}
			
			else if(distanciaVision<dist){					// si la distancia al objetivo es mayor que la distancia de vision 
			
				enemigo.speed=0;							//velocidad del enemigo 
			}
		}
	}
}