using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class controlTotal : MonoBehaviour {


	public static int contadorVelas;

	public GameObject[] velas;


	public static int contador;
	//public static GameObject[] ordenVelas;

	public static List<GameObject> ordenVelas= new List<GameObject>();
	public static List<GameObject> velasEncendidas = new List<GameObject>();
	public static bool  aviso=false;

	[SerializeField]
	private GUIStyle texto_style;
	[SerializeField]
	private GUIStyle contadorStyle;

	public static int a;


	public static bool eslaVela;

	public static  string mostrar;

	private float tiempoAviso;
	private float tiempoActual;

	public static bool textoInicio = true;
	public static bool mostrar_contador = true;

	void Awake(){

		velas = GameObject.FindGameObjectsWithTag("llama");
		
		a=0;
		contador = 0;
		tiempoActual = 0;
		tiempoAviso = 3;
		eslaVela = false;



	}

	void Start (){
		mostrar = "textoInicio";
	
		
		for(int i=0; i< velas.Length;i++){
			
			contador = Random.Range(0,velas.Length);
			
			while (ordenVelas.Contains(velas[contador])){
				contador = Random.Range(0,velas.Length);
			}
			
			ordenVelas.Add(velas[contador]);
			
		}
	
		Invoke("quitarTexto",5.0f);
	}

	void Update (){

		Debug.Log ( "ordenVelas : " +ordenVelas[a]);

		tiempoActual-=Time.deltaTime;

		texto_style.fontSize = Screen.width/40;
		contadorStyle.fontSize = Screen.width/40;

		//if( ordenVelas[a] == null)
		if(ordenVelas[a].GetComponent<encenderFarolos>().Flame.active){

			//velasEncendidas[a] = ordenVelas[a];
		
		/*	contador = Mathf.FloorToInt(Random.Range(0,cantidadVelas-0.1f));
			ordenVelas=velas[contador];
			GameObject.FindWithTag("GameController").GetComponent<menuWinner>().NumerodeLuces++;
*/			
			Debug.Log("bien");
			menuWinner.NumerodeLuces++;
			a++;

		}

	}

	void OnGUI (){

	

		switch (mostrar){

		case "textoInicio":
			GUI.Label( new Rect(Screen.width*5/12,Screen.height*1/12,Screen.width*2/12,Screen.height*1/12),"Turn on all the LIGHTS",texto_style);
			break;
		case "eslaVela":
			if(tiempoActual>=0)
			GUI.Label( new Rect(Screen.width*4/12,Screen.height*10/12,Screen.width*2/12,Screen.height*1/12),"You are safe in this area ",texto_style);
			break;
		case "NOeslaVela":
			if(tiempoActual>=0)
			GUI.Label( new Rect(Screen.width*4/12,Screen.height*10/12,Screen.width*2/12,Screen.height*1/12),"That's not the Light im looking for",texto_style);
			break;
		 default:
			break;
		}

		if(aviso){
			tiempoActual=tiempoAviso;
			aviso=false;
		}
		
	/*	if(textoInicio){
		
			GUI.Label( new Rect(Screen.width*5/12,Screen.height*1/12,Screen.width*2/12,Screen.height*1/12),"Turn on all the LIGHTS",texto_style);
		
		}
		
			tiempoActual-=Time.deltaTime;

		if(eslaVela == false && tiempoActual>=0){

				Debug.Log("Mostrar");
				GUI.Label( new Rect(Screen.width*4/12,Screen.height*10/12,Screen.width*2/12,Screen.height*1/12),"That's not the Light im looking for",texto_style);
			}
		 
		if(eslaVela == true && tiempoActual>=0){
			
			GUI.Label( new Rect(Screen.width*4/12,Screen.height*10/12,Screen.width*2/12,Screen.height*1/12),"You are safe in this area ",texto_style);

			Debug.Log("seguro");
			}
		*/
		//Contador de velas encendidas
		if(mostrar_contador)
		GUI.Label(new Rect(Screen.width*185/200, Screen.height*1/20,Screen.width*5/20,Screen.height*3/20),contadorVelas+"/"+velas.Length,contadorStyle);
	}

	void quitarTexto (){
		textoInicio=false;
		mostrar="";
	}

	void OnDestroy(){

		for(int a=0;a<velas.Length;a++){
			
			ordenVelas.Remove(velas[a]);
		}
	}


}