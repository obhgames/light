using UnityEngine;
using System.Collections;

public class ninia_anim : MonoBehaviour {

	public Transform player;
	public AnimationClip reposoNina;
	public AnimationClip atacarNina;
	private Animation _animation;

	void Start (){
		_animation=GetComponent<Animation>();
	}

	void Update (){

		if(player != null){
			if((player.position-transform.position).magnitude<10){

				GetComponent<Animation>().CrossFade(atacarNina.name);
			}
			
			else{	
				
				GetComponent<Animation>().CrossFade(reposoNina.name);
			
			}
		}
	}
}