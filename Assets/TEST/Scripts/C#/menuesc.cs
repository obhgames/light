using UnityEngine;
using System.Collections;

public class menuesc : MonoBehaviour {


	private bool  pausado ;

	public GameObject player;

//GUIStyle
	[SerializeField]
	private GUIStyle boton_style;
	[SerializeField]
	private GUIStyle fondo;

	private int a;


	void Start(){
		pausado = false;
		a = 0;

	}
	void Update (){

			boton_style.fontSize = Screen.width / 40;

		}

	void OnGUI (){



		if( Event.current.Equals (Event.KeyboardEvent("escape"))){  

			pausado=!pausado;   
		    Cursor.visible = true;    
	//		controlTotal.textoInicio = false;
		//	controlTotal.aviso = false;
			controlTotal.mostrar_contador = false;
			controlTotal.mostrar="";
		} 

		if(pausado)
		{
			Time.timeScale=0;

			
			GUI.Label( new Rect(0,0,Screen.width,Screen.height),"",fondo);
			
			if(GUI.Button( new Rect(Screen.width*5/12, Screen.height*4/12, Screen.width*2/12, Screen.height*1/12),"Resume",boton_style)  ){			
			
				player.active=true;
				Time.timeScale=1;				
				pausado=false;
				controlTotal.mostrar_contador = true;
				Cursor.visible = false; 
			}
			else{			
				player.active=false;Time.timeScale=0;
			}
			if(GUI.Button(new Rect(Screen.width*5/12, Screen.height*6/12, Screen.width*2/12, Screen.height*1/12),"Main menu",boton_style))
			{
				//Caching.CleanCache();
				Application.LoadLevel("menuInicial");
			}
			
			if(GUI.Button( new Rect(Screen.width*5/12, Screen.height*8/12, Screen.width*2/12, Screen.height*1/12),"Quit",boton_style)){
			
				Application.Quit();
			}			
		} else {player.active=true;
			Time.timeScale=1;				
			pausado=false;
			controlTotal.mostrar_contador = true;
			Cursor.visible = false; }
	}
}