using UnityEngine;
using System.Collections;

public class animacionPlayer : MonoBehaviour {


	public AnimationClip reposoAnim;
	public AnimationClip andarAnim;

	private bool horizontal;
	private bool vertical ;



	void Update (){

		horizontal = !Mathf.Approximately(Input.GetAxis("Horizontal"), 0.0f);
		vertical = !Mathf.Approximately(Input.GetAxis("Vertical"), 0.0f);

		if(horizontal|| vertical ){
		 
		 	GetComponent<Animation>().CrossFade(andarAnim.name);
		 
		}
	
		else{

			GetComponent<Animation>().CrossFade(reposoAnim.name);
		}
	}
}