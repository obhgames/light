using UnityEngine;
using System.Collections;

public class controlMuerte : MonoBehaviour {


	public static int vidas;

	public GameObject player;

	private bool  gameOver;
	private bool  Winner;

//GUIstyle
	[SerializeField]
	private GUIStyle boton_style;
	[SerializeField]
	private GUIStyle titulo_style;
	[SerializeField]
	private GUIStyle fondo;


	void Awake (){
				
		vidas=1;
		Cursor.visible = false;
		gameOver = false;
		Winner = false;
		
			
	}

	void Update (){

	//actualizando el tamaño de las fuentes
		boton_style.fontSize = Screen.width / 40;
		titulo_style.fontSize = Screen.width / 15;

		if(vidas <= 0){
		
			gameOver = true;
		}
	}

	public void renacer (){
		print("renace");
		Instantiate(player);
	}

	void OnGUI (){
		
		
		if(gameOver==true){
			
			Cursor.visible = true;
			GUI.Label( new Rect(0,0,Screen.width,Screen.height),"",fondo);
			GUI.Label ( new Rect(Screen.width*30/120, Screen.height*1/12, Screen.width*6/12, Screen.height*4/12), "YOU ARE DEAD",titulo_style);
			
			if(GUI.Button( new Rect(Screen.width*5/12, Screen.height*4/12, Screen.width*2/12, Screen.height*1/12),"Restart",boton_style)){
		
				Application.LoadLevel("colegio");
				Cursor.visible = false;
			}
			if(GUI.Button(new Rect(Screen.width*5/12, Screen.height*6/12, Screen.width*2/12, Screen.height*1/12),"Main menu",boton_style))
			{
				//Caching.CleanCache();
				Application.LoadLevel("menuInicial");
			}
			
			if(GUI.Button( new Rect(Screen.width*5/12, Screen.height*8/12, Screen.width*2/12, Screen.height*1/12),"Quit",boton_style)){

				Application.Quit();
			}
		}
	}
}