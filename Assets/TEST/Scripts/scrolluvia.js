﻿#pragma strict

var scrollSpeed : float = 0.5;

function Update () {
var offset : float = Time.time * scrollSpeed;
GetComponent.<Renderer>().material.SetTextureOffset ("_MainTex", Vector2(0,offset));
}
