
var luz : Light; //No es necesario asignarla si se añade directamente en la luz

var sonidoEncender : AudioClip;
var sonidoApagar : AudioClip;

var minTiempoEncendido : float = 0.5;
var maxTiempoEncendido : float = 15;
var minTiempoApagado : float = 0.15;
var maxTiempoApagado : float = 1;

private var actTiempo : float;

private var contador : float;

function Awake () {
	if(!luz&&gameObject.GetComponent.<Light>()){
		luz = gameObject.GetComponent.<Light>();
	}
	
	actTiempo = TiempoAleatorio (minTiempoEncendido,maxTiempoEncendido);
}

function Update () {

	contador += Time.deltaTime;

	if(contador>=actTiempo){

		if(luz.enabled){
	
			luz.enabled = false;
		
			actTiempo = TiempoAleatorio (minTiempoApagado,maxTiempoApagado);
	
		}
		else{
	
			luz.enabled = true;
	
			actTiempo = TiempoAleatorio (minTiempoEncendido,maxTiempoEncendido);
		}
		
		if(sonidoEncender&&sonidoApagar){//Si estan los dos sonidos asignados
			Sonar(luz.enabled);
		}
		
	}
}

function TiempoAleatorio (min : float , max : float) : float {


	var tiempo : float = Random.Range(min,max);
	
	contador = 0;
	
	return tiempo;

}

function Sonar (encendido : boolean ) {

	var flickerSound = new GameObject ("FlickerSound");
	
	flickerSound.transform.position = transform.position;
	
	flickerSound.AddComponent.<AudioSource>();
	
	if(encendido){
	
		flickerSound.GetComponent.<AudioSource>().clip = sonidoEncender;
        
	}
	else{
	
		flickerSound.GetComponent.<AudioSource>().clip = sonidoApagar;
	
	}
	
	flickerSound.GetComponent.<AudioSource>().Play();
	
	yield WaitForSeconds(flickerSound.GetComponent.<AudioSource>().clip.length);
	
	Destroy(flickerSound);

}
