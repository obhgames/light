﻿#pragma strict

function Awake() {

    // see if we've got game music still playing
    
    var gameMusic : GameObject = GameObject.Find("MenuMusic");
    
    if (gameMusic) {
    
        // kill game music
        
        Destroy(gameObject);
    }
    else
    {
    // make sure we survive going to different scenes
    DontDestroyOnLoad(gameObject);}
    
}